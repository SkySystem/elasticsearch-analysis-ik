package org.wltea.analyzer.dic;


import org.wltea.analyzer.help.ESPluginLoggerFactory;

import org.apache.logging.log4j.Logger;

/**
 * @program: elasticsearch-analysis-ik-master
 * @description: 执行远程词库的热更新
 * @author: Chaozi
 * @create: 2022-04-14 11:56
 **/
public class HotDictReloadThread {
    //获取词典单子实例，并执行它的reLoadMainDict方法;
    private static final Logger log = ESPluginLoggerFactory.getLogger(HotDictReloadThread.class.getName());
    public void initial(){
        while (true) {
            log.info("正在调用HotDictReloadThread...");
            Dictionary.getSingleton().reLoadMainDict();
        }
    }
}
